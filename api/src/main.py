import os
import sys
import flask
from flask_cors import CORS
from flask_httpauth import HTTPTokenAuth
from flask import jsonify
import json
import threading
import history
import scriptdirs
import log
import dir

app = flask.Flask(__name__)
auth = HTTPTokenAuth(scheme='Bearer')
cors = CORS(app, resources=r'/api/*', headers='Content-Type')

tokens = {
    "secret-token-1": "api-user"
}

@auth.verify_token
def verify_token(token):
    if token in tokens:
        return tokens[token]

@app.route('/')
def index():
    return "Home Page"


@app.route('/api/controller/restart', methods=['GET'])
def restart_program():
    """
    Restarts the ScriptControl
    """
    python = sys.executable
    os.execl(python, python, * sys.argv)

@app.route('/api/dir/list', methods=['GET'])
def get_dir_list():
    """
    DONE
    Return a list of the dir
    """
    #List all unused scripts that are in the ./scripts/ dir
    list_all = []
    for key in dir_list:
        list_all_info = {
            'name': key,
            'directory': dir_list[key].is_directory(),
            'total_scripts': dir_list[key].get_total_scripts(),
        }
        list_all.append(list_all_info)
    return flask.jsonify({'all': list_all, 'return': True})
    #return jsonify(list_all)

@app.route('/api/dir/list/update', methods=['GET'])
def update_dir_list():
    #rescan scripts dir
    add_dirs()
    return flask.jsonify({'output': 'List Updated', 'return': True})

@app.route('/api/script/list', methods=['GET'])
def get_script_list():
    """
    DONE
    Return a list of the scripts
    """
    #List all unused scripts that are in the ./scripts/ dir
    list_all = []
    for key in script_dir_list:
        list_all_info = {
            'name': key,
            'path': script_dir_list[key].get_path(),
            'parrent_dir': script_dir_list[key].get_parrent_dir(),
            'uptime': script_dir_list[key].get_uptime(),
            'last_run': script_dir_list[key].get_last_run(),
            'running': script_dir_list[key].is_running(),
            'enabled': script_dir_list[key].is_enabled(),
            'trigger': script_dir_list[key].get_trigger_type(),
            'trigger_setting': script_dir_list[key].get_trigger_settings()
        }
        list_all.append(list_all_info)
    return flask.jsonify({'all': list_all, 'return': True})


@app.route('/api/dir/<dir_name>/list', methods=['GET'])
def get_dir_script_list(dir_name):
    """
    DONE
    Return a list of the scripts
    """
    #List all unused scripts that are in the ./scripts/ dir
    list_all = []
    for key in script_dir_list:
        
        path = str(script_dir_list[key].get_path())
        path = path.split('/')
        path = ' '.join(path).split()
        last_element = path[-1:][0]
        if dir_name == last_element:
            list_all_info = {
                'name': key,
                'path': script_dir_list[key].get_path(),
                'parrent_dir': script_dir_list[key].get_parrent_dir(),
                'uptime': script_dir_list[key].get_uptime(),
                'last_run': script_dir_list[key].get_last_run(),
                'running': script_dir_list[key].is_running(),
                'enabled': script_dir_list[key].is_enabled(),
                'trigger': script_dir_list[key].get_trigger_type(),
                'trigger_setting': script_dir_list[key].get_trigger_settings()
            }
            list_all.append(list_all_info)
    return flask.jsonify([{'all': list_all, 'return': True}])
    #return jsonify(list_all)

@app.route('/api/dir/<dir_name>/list/update', methods=['GET'])
def update_dir_script_list(dir_name):
    #rescan scripts dir
    add_dir_scripts()
    return flask.jsonify({'output': 'List Updated', 'return': True})

@app.route('/api/dir/<dir_name>/<script_name>/run', methods=['GET'])
def script_run(script_name,dir_name):
    """
    DONE
    Adds script to the scriptList and creates an object
    """
    output, return_val = script_dir_list[script_name].run()
    log_.warning('Run ('+script_name+'): '+str(return_val)+' - '+output)
    return flask.jsonify({'output': output, 'return': return_val})

@app.route('/api/dir/<dir_name>/<script_name>/enable', methods=['GET'])
def script_enable(script_name,dir_name):
    """
    DONE
    Enables script if configured
    """
    output, return_val = script_dir_list[script_name].set_enabled()
    log_.warning('Enable ('+script_name+'): '+str(return_val)+' - '+output)
    return flask.jsonify({'output': output, 'return': return_val})


@app.route('/api/dir/<dir_name>/<script_name>/disable', methods=['GET'])
def script_disable(script_name,dir_name):
    """
    DONE
    Disables script
    """
    output, return_val = script_dir_list[script_name].set_disabled()
    log_.warning('Disable ('+script_name+'): '+str(return_val)+' - '+output)
    return flask.jsonify({'output': output, 'return': return_val})


@app.route('/api/dir/<dir_name>/<script_name>/stop', methods=['GET'])
def script_stop(script_name,dir_name):
    """
    DONE
    Stops script
    """
    output, return_val = script_dir_list[script_name].stop()
    log_.warning('Disable ('+script_name+'): '+str(return_val)+' - '+output)
    return flask.jsonify({'output': output, 'return': return_val})


@app.route('/api/dir/<dir_name>/<script_name>/setting/<string:setting>/<int:value>', methods=['GET'])
def script_set_trigger(script_name, setting, value,dir_name):
    """
    DONE
    Set script trigger settings
    """
    output, return_val = script_dir_list[script_name].set_trigger_setting(setting, value)
    log_.warning('Set Trigger ('+script_name+'): '+str(return_val)+' - '+output)
    return flask.jsonify({'output': output, 'return': return_val})


@app.route('/api/dir/<dir_name>/<script_name>/output/hist/<string:amount>', methods=['GET'])
@app.route('/api/dir/<dir_name>/<script_name>/output/hist/<string:amount>/<int:limit>', methods=['GET'])
def get_output_history(script_name,dir_name, amount, limit=1):
    """
    Get the history of a scripts output
    all: gets all output sorted by newest first
    last: gets latest output, if a limit is passed it will get the last n outputs
    """
    return_val = True
    if amount.lower() == "all":
        output = hist.get_all(script_name)
    elif amount.lower() == "last":
        output = hist.get_last(script_name, limit)
    else:
        output = "Invalid call"
        return_val = False
 
    log_.warning('Output History ('+script_name+'): '+str(return_val)+' - '+str(output))
    return flask.jsonify({'output': output, 'return': return_val})


@app.route('/api/dir/<dir_name>/<script_name>/output/live', methods=['GET'])
def get_output_live(script_name,dir_name):
    """
    DONE
    Get the live output
    """
    output, return_val = script_dir_list[script_name].get_output()
    log_.warning('Output Live ('+script_name+'): '+str(return_val)+' - '+str(output))
    return flask.jsonify({'output': output, 'return': return_val})


@app.route('/api/dir/<dir_name>/<script_name>/next_run', methods=['GET'])
def get_next_run(script_name,dir_name):
    output, return_val = script_dir_list[script_name].get_next_run()
    return flask.jsonify({'output': output, 'return': return_val})


def save_data():
    """
    Saves data about the script_list every 5 seconds
    """
    save_list = {}
    for key in script_dir_list:
        save_list[key] = {
            'trigger_settings': script_dir_list[key].get_trigger_settings(),
            'enabled': script_dir_list[key].is_enabled(),
            'next_run': script_dir_list[key].get_next_run()
        }
    with open('data.json', 'w') as outfile:
        json.dump(save_list, outfile)
    threading.Timer(5, save_data).start()

def load_data():
    with open('data.json') as data_file:
        data = json.load(data_file)
    return data

def add_dir_scripts():
    try:
        saved = load_data()
    except:
        saved = {}
    for root,dirnames,filenmaes in os.walk('./scripts/'):
        for filename in filenmaes:
            file_name = os.path.splitext(os.path.basename(filename))
            if file_name[1] == ".py" and filename != "__init__.py" and filename != "template.py":
                script_name = file_name[0]
                if script_name not in script_dir_list.keys():
                    script_dir_list[script_name] = scriptdirs.ScriptDirs(script_name,root)
                    if script_name in saved: #then read in old values
                        #add back any settings
                        if script_dir_list[script_name].get_trigger_type() != 'call':
                            saved_setting = saved[script_name]['trigger_settings']
                            for setting in saved_setting:
                                script_dir_list[script_name].set_trigger_setting(setting, saved_setting[setting])
                        #set to last enabled state
                        if saved[script_name]['enabled']:
                            script_dir_list[script_name].set_enabled()
                                               
def add_dirs():
    for dir_name in os.scandir("./scripts/dirs/"):
        if dir_name.is_dir():
            dir_name = dir_name.name
            if dir_name not in dir_list.keys():
                dir_list[dir_name] = dir.Dir(dir_name)

if __name__ == '__main__':
    #set up general log file
    log_ = log.Log('./_log.sqlite')
    #set up script history log
    hist = history.History('./scripts/_history.sqlite')
    #dict to store scripts data
    script_dir_list = {}
    dir_list = {}
    add_dirs()
    add_dir_scripts()
    save_data()
    log_.error("Main script started")
    app.run(port=5000, host='0.0.0.0', debug=False)