import os

class Dir:
    """
    Handles all the data for the directory
    """
    def __init__(self, dir_name):
        self._name = dir_name
        self._directory = True
        self._path = (f'./scripts/dirs/{dir_name}')
        self._total_scripts = 0
        self._enabled = False
        
    def get_total_scripts(self):
        """
        Return the total number of scripts
        """
        total_scripts = 0
        for each_file in os.listdir(self._path):
            if each_file == ".py" and each_file != "__init__.py" and each_file != "template.py":
                if os.path.isfile(os.path.join(self._path, each_file)):
                    total_scripts+=1
                
                self._total_scripts = total_scripts
            
        return self._total_scripts
    
    def get_path(self):
        """
        Returns the Path
        """
        return self._path
    
    def is_directory(self):
        """
        Return True if Directory status
        """
        return self._directory
        