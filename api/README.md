# Python Control Interface API



API
---
Returns a list of scripts and their info

    /script/list
    
Rescans the scripts dir and adds new scripts to the list

    /script/list/update
    
Restarts the main script controller

    /controller/restart


All calls below will return a JSON object with:

  - **output** which contains the response text
  - **return** which will return a bool with the success of the command sent
  
Enables the script to be run

    /script/<name>/enable
    
Disables the script from running

    /script/<name>/disable

Runs the script if enabled

    /script/<name>/run
    
Returns the current output of the script

    /script/<name>/output/live
    
Stops the script during the current call where `if not self._running: break` is found in the loop

    /script/<name>/stop