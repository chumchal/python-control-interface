<?php
//include('./functions.php');

function str2cml($text){ 
    return preg_replace('/\s/', '', $text);
}

function get_module_list(){
    $module_dir    = './modules';
    $modules = scandir($module_dir);
    $module_dict = array();
    foreach($modules as $module) {
        if($module != '.' && $module != '..'){
            $base_dir = $module_dir.'/'.$module.'/';
            $config = parse_ini_file($base_dir.'config.ini');
            $id = str2cml($config['name']);
            $index = $base_dir.$config['index'];
            $module_dict[$id] = ['id' => $id,
                                 'index' => $index,
                                 'config' => $config
            ];
        }
    }
    return json_encode($module_dict);
} //END get_module_list()

?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Main Control Interface</title>
    <link href="./assets/vendor/css/lato-fonts.css" rel='stylesheet' type='text/css'>
    <link href="./assets/vendor/css/jquery-ui.css" rel='stylesheet' type='text/css'>
    <link href="./assets/css/styles.css" rel="stylesheet" type="text/css">
    <link href="assets/css/jquery.contextMenu.css" rel="stylesheet" type="text/css">

    <script src="./assets/vendor/js/jquery.min.js"></script>
    <script src="./assets/vendor/js/jquery-ui.min.js"></script>
    <script src="./assets/vendor/js/underscore-min.js"></script>
    <script src="./assets/js/jquery.contextMenu.js"></script>
    <script src="./assets/js/init.js"></script>

    <script>
        var moduleList = <?php echo get_module_list(); ?>;
    </script>

</head>
<body>

<div id="modulesContainer">
</div>

</body>
</html>